import os
import sys
import time
import shutil
# import ROOT


def makeIfNotYetThere(dirname):
    if os.path.isdir(dirname):
        return dirname
    else:
        try:
            os.makedirs(dirname)
            return dirname
        except OSError:
            print("Directory %s could not be created!", dirname)
            raise


def firstNotYetExisting(dirname):
    if os.path.isdir(dirname):
        for iteration in range(1, 999):
            NewDirName = dirname + "_%03d" % iteration
            if not os.path.isdir(NewDirName):
                dirname = NewDirName
                break
    try:
        os.makedirs(dirname)
    except OSError:
        print("Directory %s could not be created!", dirname)
        raise
    return dirname


class BNLCondorJob:

    def __init__(self, name, SubmitScript, outDir, logDir, tmpLogDir, ParentJobs=[]):
        self.ParentJobs = ParentJobs
        self.SubmitScript = SubmitScript
        self.outDir = outDir
        self.logDir = logDir
        self.tmpLogDir = tmpLogDir
        self.Name = name

    def getOutDir(self):
        return makeIfNotYetThere(self.outDir + "/" + self.Name + "/")

    def getLogDir(self):
        return makeIfNotYetThere(self.logDir + "/" + self.Name + "/")

    def getTmpLogDir(self):
        return makeIfNotYetThere(self.tmpLogDir + "/" + self.Name + "/")

    def makeDirs(self):
        self.getOutDir()
        self.getLogDir()
        self.getTmpLogDir()

    ## TO BE IMPLEMENTED IN THE CONCRETE SPECIALISATION
    def getNOutFiles(self):
        return 0

    ## TO BE IMPLEMENTED IN THE CONCRETE SPECIALISATION
    ## SHOULD RETURN A DICT {FILE TYPE AS STRING: LIST OF FILE NAMES}
    def getOutputs(self):
        return {}

    ## return the names of the parent jobs
    def getParents(self):
        return self.ParentJobs

    def getJobName(self):
        return self.Name

    # generate the string for a DAG. This makes some key variables available
    def generateDAGBaseSnippet(self):
        DagCmd = ""
        DagCmd += "JOB %s %s \n" % (self.getJobName(), self.SubmitScript)
        DagCmd += "VARS %s outDir=\"%s\"\n" % (self.getJobName(), self.getOutDir())
        DagCmd += "VARS %s logDir=\"%s\"\n" % (self.getJobName(), self.getLogDir())
        DagCmd += "VARS %s tmpLogDir=\"%s\"\n" % (self.getJobName(), self.getTmpLogDir())
        for parent in self.getParents():
            DagCmd += "PARENT %s CHILD %s \n" % (parent.getJobName(), self.getJobName())
        return DagCmd


###
### Submit a set of jobs implementing the BNLCondorJob interface.
### Will build a DAG and automatically account for configured dependencies
###
def SubmitJobs(submissionName, items, DagDir):

    TheDag = ""
    for item in items:
        # make sure the folders expected by the jobs exist
        item.makeDirs()
        # and book into dag
        TheDag += "########### " + item.Name + " ##########\n"
        TheDag += item.generateDAGSnippet()
        TheDag += "###############################\n\n"

    with open("%s/%s.dag" % (DagDir, submissionName), "w") as fout:
        fout.write(TheDag)

    os.system("condor_submit_dag  %s/%s.dag" % (DagDir, submissionName))


def getJobSnippet_singleJob():
    return """
    Universe        = vanilla

    Notification    = Never
    Executable      = $(TheExecutable)
    Arguments       = $(TheArguments) 
    # Get the environment from the submission host?

    GetEnv          = True

    # Only set these if you want to stream files to/from the submit machine
    should_transfer_files = YES
    when_to_transfer_output = ON_EXIT
    transfer_input_files = $(TheExecutable),$(InFileList)

    transfer_output_files = $(OutputFile)

    transfer_output_remaps = "$(OutputFile)=$(outDir)/$(OutputFile).$(process)"

    # request_disk   = 8 GB
    # request_memory = 2.2 GB
    # The standard output and error of your job's executable
    Output          = $(logDir)/Out.$(cluster).$(process)
    Error           = $(logDir)/Err.$(cluster).$(process)

    # Should not need to transfer input files

    # Don't put the user-log file on NFS disk as it can be slow,
    # you can create a temporary directory on the submit node
    # !! NOTE: your home directory is likely on NFS
    Log             = $(tmpLogDir)/log.$(cluster).$(process)

    Rank  = Memory 

"""


if __name__ == "__main__":
    pass
