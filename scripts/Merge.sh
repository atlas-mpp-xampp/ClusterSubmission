#!/bin/bash
echo "###############################################################################################"
echo "                     Environment variables"
echo "###############################################################################################"
export
echo "###############################################################################################"
echo " "
if [[ ${HOSTNAME} == ct* ]]; then
    #tmp_folder=/dev/shm/$USER/${SLURM_ARRAY_JOB_ID}_${SLURM_ARRAY_TASK_ID}/
    tmp_folder=/ptmp/mpp/$USER/Cluster/TMP/${SLURM_ARRAY_JOB_ID}_${SLURM_ARRAY_TASK_ID}/
    rm -rf $tmp_folder
    mkdir -p $tmp_folder
    echo "true"
else
    tmp_folder=${TMPDIR}
fi

echo "tmp_folder set to $tmp_folder"
echo "Hostname $HOSTNAME"

if [ -z "${ATLAS_LOCAL_ROOT_BASE}" ]; then
    echo "###############################################################################################"
    echo "                    Setting up the environment"
    echo "###############################################################################################"
    cd ${TMPDIR}
    export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
    echo "Setting Up the ATLAS Enviroment:"
    echo "source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh"
    source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
    echo "asetup ${OriginalProject},${OriginalPatch},here"
    asetup ${OriginalProject},${OriginalPatch},here
else
    echo "Assuming your AthAnalysis environment has been set up"
fi

if [ -f "${ClusterControlModule}" ]; then
    source ${ClusterControlModule}
fi

ID=$(get_task_ID)
echo "Got task ID ${ID}"
MergeList=""
if [ -f "${JobConfigList}" ]; then
    echo "MergeList=$(sed -n \"${ID}{p;q;}\" ${JobConfigList})"
    MergeList=$(sed -n "${ID}{p;q;}" ${JobConfigList})
else
    echo "ERROR: List ${JobConfigList} does not exist"
    send_to_failure
fi

if [ ! -f "${MergeList}" ]; then
    echo "########################################################################"
    echo " ${MergeList} does not exist"
    echo "########################################################################"
    send_to_failure
    exit 100
fi
To_Merge=""
while read -r M; do
    FindSlash=${M##*/}
    if [ ! -d "${M/$FindSlash/}" ]; then
        echo "The Dataset ${M} is stored on dCache.  Assume that the file is healthy"
    elif [ ! -f "${M}" ]; then
        echo "File ${M} does not exist. Merging Failed"
        echo "###############################################################################################"
        echo "                    Merging failed"
        echo "###############################################################################################"
        send_to_failure
        exit 100
    fi
    To_Merge="${To_Merge} ${M}"
done <"${MergeList}"

if [ -f "${OutFileList}" ]; then
    echo "OutFile=$(
        sed -n \"${ID}{p;q;}\" ${OutFileList})"
    OutFile=$(sed -n "${ID}{p;q;}" ${OutFileList})
fi
if [ -f "${OutFile}" ]; then
    echo "Remove the old ROOT file"
    rm -f ${OutFile}
fi
echo "hadd ${tmp_folder}/tmp.root ${To_Merge}"
hadd ${tmp_folder}/tmp.root ${To_Merge}
if [ $? -eq 0 ]; then
    ls -lh
    echo "mv ${tmp_folder}/tmp.root ${OutFile}"
    mv ${tmp_folder}/tmp.root ${OutFile}
    echo "###############################################################################################"
    echo "                        Merging terminated successfully"
    echo "###############################################################################################"
else
    echo "###############################################################################################"
    echo "                    Merging failed"
    echo "###############################################################################################"
    send_to_failure
    exit 100
fi
